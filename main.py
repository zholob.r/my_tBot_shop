import telebot
from telebot import types
from os import listdir              #допоможе завантажити необхідні файли
from random import randint

bot = telebot.TeleBot('6925142536:AAEOFMFpDlNF4tC2fDzQ6S39bwALhYuUcfg')     #створення зміної для роботи з ботом

#глобальний список з id мессенджів товарів для видалення (старі)
list_current_id = []

def del_mess(chat_id):
    '''
    Ф-я видалення старих товарів
    :param chat_id: ід чату з якого видаляти
    id_mes: ід самого повідомлення, яке зберігається в глобальному списку list_current_id
    :return: 0, але очищаємо глобальний list_current_id, який містить ід товарів
    '''
    global list_current_id
    for id_mes in list_current_id:
        bot.delete_message(chat_id, id_mes)             #видалення з чату(chat_id) повідомлення яке має id_mes
    list_current_id.clear()


#ф-я завантаження ресурсів товару(читання данних з ресурсів)
# різних типів, як текст так і зображення
def reader(list_p, name_p, adres, mode):
    logic = False if 'rb' in mode else True
    with open(f'{adres}/{[el for el in list_p if name_p in el and (".txt" in el)==logic][0]}',
        mode,
        encoding = 'utf-8' if logic else None) as fl:

        return fl.read()


#універсальна ф-я завантаження товарів залежно від напрямку
def load_product(address, chat_id):
    '''
    Уіверсальна ф-я завантаження товарів залежно від напрямку
    :param adress:  - шлях де саме зберігається в наших res картинкки та опис товару
    :param chat_id:  ід чату  це виводимо товари
    глобальна list_current_id отримує message_id після виведення повідомлення в боті
    '''
    global list_current_id
    # створення кнопки придбання товару
    inl_pay = types.InlineKeyboardMarkup().add(types.InlineKeyboardButton('Придбати', callback_data='pay'))

    # завантаження файлів ресурсів
    list_product = listdir(address)

    list_uniq_name = list({el.split('.')[0] for el in list_product})

    for name_prod in list_uniq_name:
        list_current_id.append(
            # відправка фото та опису товару в мессенджер
            bot.send_photo(chat_id,
                           reader(list_product, name_prod, address, 'rb'),
                           caption=reader(list_product, name_prod, address, 'r'),
                           reply_markup=inl_pay
                           ).message_id
        )

#об'єкто головної підекранної клавіатури
main_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True).add('Каталог напрямків ІТ', 'Поділитися ботом')


# декоратор обробки месседж команд
@bot.message_handler(commands = ['start', 'help'])
def get_commands(message):
    if message.text == '/start':
        bot.send_photo(message.chat.id, open("res/cat.jpg", "rb"),
                       'Вітаю, це тестовий магазин курсів\U0001F601 ',  #\U0001F601 == 😁
                       reply_markup= main_keyboard)
        # bot.send_message(message.chat.id, 'Вітаю, це тестовий магазин курсів\U0001F601 ',            #\U0001F601 == 😁
        #                  reply_markup= main_keyboard)



#Декоратор обробки текстового месседжу (тицнення однієї з кнопок підекранної клавіатури)
@bot.message_handler(content_types='text')
def get_text(message):
    global list_current_id
#обробка натицення "Каталог товарів" на головній клавіатурі
    if message.text == 'Каталог напрямків ІТ':
        inl_keyb = types.InlineKeyboardMarkup()             #об'єкт інлайн клавіатури

        # додавання кнопочок в інлайн клавіатуру додає в 1 рядок
        inl_keyb.add(types.InlineKeyboardButton('Програмування', callback_data='cat1'),
                     types.InlineKeyboardButton('Тестування', callback_data='cat2'))
        #додавання кнопочки в 2й рядок
        inl_keyb.add(types.InlineKeyboardButton('Дизайн', callback_data='cat3'))
    #вивід клавіатури користувачу
        list_current_id.append(
                bot.send_message(message.chat.id,
                             'Оберіть напрямок розвитку в ІТ:',
                             reply_markup=inl_keyb).message_id
        )


#Декоратор обробки тицнення інлайн кнопок вже працюємо не з об'єктом message
#а з об'єктом query який теж містить дофіга інфи і теж словник
@bot.callback_query_handler(lambda a: True)
def get_query(query):
    global list_current_id
#Обробка події натицення кнопки 'Програмування', callback_data='cat1'
    if query.data == 'cat1':
        del_mess(query.message.chat.id)

        inl_keyb = types.InlineKeyboardMarkup()  # об'єкт нової інлайн клавіатури
        inl_keyb.add(types.InlineKeyboardButton('Java', callback_data='java'),
                     types.InlineKeyboardButton('C#', callback_data='csharp'))
        # додавання кнопочки в 2й рядок
        inl_keyb.add(types.InlineKeyboardButton('Python', callback_data='python'))
        # вивід клавіатури користувачу через query.message!
        list_current_id.append(
                bot.send_message(query.message.chat.id,
                         'Оберіть мову програмування:',
                         reply_markup=inl_keyb).message_id
        )
#!!ОБРОБКА натицнення підкатегорії і вивід товару теж в цьому блоці!
#'Java', callback_data='java'
    elif query.data == 'java':
        #алгоритм завантаження ресурсів товарів та клвіатур
        address = f'res/development/java'
        del_mess(query.message.chat.id)                     #dвидаляємо старі товари що виводились
        load_product(address, query.message.chat.id)        #виводимо товари

    #'Придбати', callback_data='pay'
    elif query.data == 'pay':
        #алгоритм створення ІД коду придбаного товару
        id_code = '#'
        for i in range(6):
            id_code += f'{randint(0,9)}'

        #вспливаюча підказка
        bot.answer_callback_query(query.id, 'Товар придбано!')
        #повідомлення про замовлення
        bot.send_message(query.message.chat.id,
                         f""" Ваш номер замовлення: {id_code} 
Курс: ...
Впродовж робочого дня менеджер зв'яжеться з Вами"""
                         )
    elif query.data == 'cat3':
        ...         #ля прикладу логіка оброки 'Дизайн', callback_data='cat3'



bot.polling()      #Ф-я яка постійно звертається до серверу тг та дізнається чи не написали в бот щось,
                   #але мінус в тому, що через 30-40 хв сервера тг відключать бот. Завжди має бути в кінці коду
